// Thanks to http://deglos.com/blog/2010/12/21/building-wysiwyg-plugin-drupal
// Thanks to http://stackoverflow.com/questions/11328681/how-to-block-editing-on-certain-part-of-content-in-ckeditor-textarea

/**
 * This is NOT a true agnostic WYSIWYG_API plugin, it only works on CKEditor.
 */

(function ($) {

Drupal.wysiwyg.plugins['lock'] = {

  /**
   * Lock all elements with 'lock'  class on them.
   */
  attach: function(content, settings, instanceId) {
    // We have only an HTML string, not a DOM. Invent one.
    var dom = $('<div>' + content + '</div>');
    $('.locked', dom).attr('contentEditable', 'false');
    return dom.html();
  },

  /**
   * Remove my contenteditable attributes again.
   */
  detach: function(content, settings, instanceId) {
    var dom = $('<div>' + content + '</div>');

    // $('.locked', $content).removeAttr('contentEditable');
    // That .. causes errors this time (yet did not during invoke)?
    // Failed to set the 'contentEditable' property on 'HTMLElement': The value provided ('') is not one of 'true', 'false', 'plaintext-only', or 'inherit'.
    // Looks like jquery got something wrong.
    //
    // Set it in the dom directly, avoiding jquerys attr().
    $('.locked', dom).each( function(index) {
      this.removeAttribute('contenteditable');
    });

    return dom.html();
  },

  /**
   * Return whether the passed node belongs to this plugin.
   *
   * @param node
   *   The currently focused DOM element in the editor content.
   */
  isNode: function(node) {
    // Used to tell the wysiwyg whether we want to take responsibility
    // for actions on this node.

    // Most of the time you can't click directly on the container div,
    // so need to search context to see if you clicked *inside* one.

    // Is self or parents locked already.
    if ($(node).closest('.locked').length) {
      console.log('This content is locked from editing');
      // Great, we can just set contenteditable!!
      // This is is just backup for the .attach() func AND the
      // explicit toggle that may also be getting applied.
      // Other edit actions may have interfered already.
      $(node).closest('.locked').attr('contenteditable', false);
    }
  },


  /**
   * Execute the button - Toggle Lock on the current div.
   *
   * Invoke gets triggered somewhere inside the ckeditor.init() routine.
   *
   * Invoke is called when the toolbar button is clicked.
   *
   * @param editorName the ckeditor id
   */
  invoke: function(data, PluginSettings, instanceID) {
    // Find the nearest parent.
    // Add 'locked' to it.
    var element = $(this.getSelectedElement());

    if (! element.length) {
      console.log('hit the top without finding a wrapper');
      return;
    }

    element.toggleClass('locked');
    if (element.hasClass('locked')) {
      element.attr('contenteditable', false);
    }
    else {
      // Rather than peppering this attribute everywhere, just unset it.
      console.log(element);
      element.removeAttr('contenteditable');
    }
    // Those actions just hit the DOM UI directly, but did not technically
    // commit any change to the 'text' markup stored internally?
    // Need to transfer the change into storage as an extra step.
    //var editor = Drupal.wysiwyg.instances[instanceID];
    //console.log(editor.html());
    //console.log(this.document.html());

  },

  /**
   * Helper to find the element that's highlighted or being edited.
   *
   * If something in the parent chain is already locked, select that.
   * If nothing in the parent chain is locked, select the closest block level
   * element.
   *
   * If it returns NULL, it has reached the top of the document, so you probably
   * don't want to use that.
   *
   */
  getSelectedElement: function(editor) {
    try {
      if (! editor) {
        editor = CKEDITOR.currentInstance;
      }
      var selection = editor.getSelection();
      var node = selection.getStartElement().$;

      // currentElement is a jquery handle
      // Look for any container that is locked.
      var currentElement = $(node).closest('.locked');
      if (currentElement.length) {
        return currentElement;
      }

      // Nothing was locked, so find the nearest parent instead.
      currentElement = node;
      var blocks = ['DIV', 'P', 'FIGURE'];
      while (currentElement) {
        console.log(currentElement.nodeName);
        for (var i in blocks) {
          console.log(blocks[i]);
          if (currentElement.nodeName == blocks[i]) {
            return currentElement;
          }
        }
        currentElement = currentElement.parentElement;
      }

      return NULL;
    }
    catch( e ) {
      console.log('Had trouble selecting an element for locking. ' + e.message);
      return null;
    }
  },


  /**
   * I do NOT do anything with attach and detach calls,
   * My markup is round-trip safe! Because I am cool.
   */

};

})(jQuery);
