<?php
/**
 * @file
 * Wysiwyg API integration for embedder elements.
 */

/**
 * Implementation of <module_name>_<plugin_name>_plugin().
 */
function wysiwyg_content_lock_lock_plugin() {
  $plugins['lock'] = array(
    'title' => t('Lock'),
    'vendor url' => 'http://drupal.org/project/wysiwyg_content_lock',
    'icon file' => 'lock-icon.gif',
    'icon title' => t('Protect this block from editing'),
    'settings' => array(),
  );
  return $plugins;
}
